package no.noroff.Assignment6.models;

public class CustomerCountry {
    private String country;
    private int total;
    public CustomerCountry(String country, int total){
        this.country = country;
        this.total = total;
    }
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
