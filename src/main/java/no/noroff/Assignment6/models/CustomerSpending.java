package no.noroff.Assignment6.models;

public class CustomerSpending {
    private int id;
    private String FirstName;
    private String LastName;
    private int totalSpending;

    public CustomerSpending( int id, String firstName, String lastName, int totalSpending) {
        this.id = id;
        FirstName = firstName;
        LastName = lastName;
        this.totalSpending = totalSpending;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getTotalSpending() {
        return totalSpending;
    }

    public void setTotalSpending(int totalSpending) {
        this.totalSpending = totalSpending;
    }
}
