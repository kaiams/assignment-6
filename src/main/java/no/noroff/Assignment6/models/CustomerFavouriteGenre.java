package no.noroff.Assignment6.models;

public class CustomerFavouriteGenre {
    private String name;
    private int total;

    public CustomerFavouriteGenre(String name, int total){
        this.name = name;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
