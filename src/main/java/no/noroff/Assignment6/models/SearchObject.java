package no.noroff.Assignment6.models;

public class SearchObject {
    private String name;

    public SearchObject() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
