package no.noroff.Assignment6.data_access;

import no.noroff.Assignment6.models.Customer;
import no.noroff.Assignment6.models.CustomerCountry;
import no.noroff.Assignment6.models.CustomerFavouriteGenre;
import no.noroff.Assignment6.models.CustomerSpending;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class CustomerRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            this.conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(createCustomer(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customers;
    }

    public Customer getCustomerById(int id) {
        Customer customer = null;
        try {
            this.conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer WHERE CustomerId =?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = createCustomer(resultSet);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customer;
    }

    public ArrayList<Customer> getCustomersWithLimitAndOffset(int limit, int offset) {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            this.conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer LIMIT ? OFFSET ?");
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(createCustomer(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customers;
    }

    public ArrayList<Customer> getCustomersByName(String name) {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            this.conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer WHERE FirstName LIKE ?");
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(createCustomer(resultSet));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customers;
    }

    public Customer addCustomer(Customer customer) {
        try {
            this.conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO customer (FirstName,LastName,Country,PostalCode,Phone,Email) VALUES (?,?,?,?,?,?)");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customer;
    }

    public Customer updateCustomer(int id, Customer updatedCustomer) {
        try {
            this.conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE customer SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            preparedStatement.setString(1, updatedCustomer.getFirstName());
            preparedStatement.setString(2, updatedCustomer.getLastName());
            preparedStatement.setString(3, updatedCustomer.getCountry());
            preparedStatement.setString(4, updatedCustomer.getPostalCode());
            preparedStatement.setString(5, updatedCustomer.getPhoneNumber());
            preparedStatement.setString(6, updatedCustomer.getEmail());
            preparedStatement.setInt(7, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return getCustomerById(id);
    }

    public ArrayList<CustomerCountry> getNumberOfCustomersToCountry() {
        ArrayList<CustomerCountry> customerCountries = new ArrayList<>();

        try{
            this.conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Country, COUNT(CustomerId) Total FROM Customer GROUP BY Country ORDER BY Total DESC");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                customerCountries.add(new CustomerCountry(
                        resultSet.getString("Country"),
                        resultSet.getInt("Total")));
            }

        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            try{
                conn.close();
            }
            catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return customerCountries;
    }

    public ArrayList<CustomerSpending> getHighestSpenders() {
        ArrayList<CustomerSpending> customers = new ArrayList<>();

        try {
            this.conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT C.CustomerId, C.FirstName, C.LastName, SUM(I.Total) Total from Customer C join Invoice I on C.CustomerId = I.CustomerId Group By C.CustomerId ORDER BY Total desc");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                customers.add(
                        new CustomerSpending(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getInt("Total"))
                        );
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customers;
    }

    public ArrayList<CustomerFavouriteGenre> getFavouriteGenreByCustomerId(int id) {
        ArrayList<CustomerFavouriteGenre> favouriteGenres = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT G.Name Genre, count(G.GenreId) Total FROM Customer C" +
                            " INNER JOIN Invoice I on C.CustomerId = I.CustomerId" +
                    " INNER JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId INNER JOIN Track T on T.TrackId = IL.TrackId" +
                    " INNER JOIN Genre G on G.GenreId = T.GenreId" +
                    " WHERE C.CustomerId = ?" +
                    " GROUP BY G.GenreId" +
                    " ORDER BY Total desc ");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            int max = -1;
            while(resultSet.next()){
                CustomerFavouriteGenre customerFavouriteGenre = new CustomerFavouriteGenre(resultSet.getString("Genre"),
                        resultSet.getInt("Total"));
                if(customerFavouriteGenre.getTotal() < max){
                    break;
                }
                max = customerFavouriteGenre.getTotal();
                favouriteGenres.add(customerFavouriteGenre);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            try {
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return favouriteGenres;
    }

    private Customer createCustomer(ResultSet resultSet) throws SQLException {
        return new Customer(
                resultSet.getInt("CustomerId"),
                resultSet.getString("FirstName"),
                resultSet.getString("LastName"),
                resultSet.getString("Country"),
                resultSet.getString("PostalCode"),
                resultSet.getString("Phone"),
                resultSet.getString("Email")
        );
    }
}
