package no.noroff.Assignment6.data_access;

import no.noroff.Assignment6.models.Artist;
import no.noroff.Assignment6.models.Genre;
import no.noroff.Assignment6.models.Song;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class MusicRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public ArrayList<Song> get5RandomSongs() {
        ArrayList<Song> songs = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Name, Composer FROM TRACK ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                songs.add(new Song(
                        resultSet.getString("Name"),
                        resultSet.getString("Composer")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return songs;
    }

    public ArrayList<Genre> get5RandomGenres() {
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Name FROM GENRE ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                genres.add(new Genre(
                        resultSet.getString("Name")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return genres;
    }

    public ArrayList<Artist> get5RandomArtists() {
        ArrayList<Artist> artists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Name FROM Artist ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                artists.add(new Artist(
                        resultSet.getString("Name")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return artists;
    }

    public ArrayList<Song> getSongsBySearch(String search) {
        ArrayList<Song> songs = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Name, Composer FROM Track WHERE Name LIKE ?");
            preparedStatement.setString(1, search + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                songs.add(new Song(
                        resultSet.getString("Name"),
                        resultSet.getString("Composer")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return songs;
    }



}
