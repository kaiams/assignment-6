package no.noroff.Assignment6.controllers;

import no.noroff.Assignment6.data_access.CustomerRepository;
import no.noroff.Assignment6.data_access.MusicRepository;
import no.noroff.Assignment6.models.Artist;
import no.noroff.Assignment6.models.Genre;
import no.noroff.Assignment6.models.SearchObject;
import no.noroff.Assignment6.models.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;

@Controller
public class ViewController {

    @Autowired
    MusicRepository musicRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homeView(Model model) {
        ArrayList<Song> randomSongs = musicRepository.get5RandomSongs();
        ArrayList<Genre> randomGenres = musicRepository.get5RandomGenres();
        ArrayList<Artist> randomArtists = musicRepository.get5RandomArtists();
        model.addAttribute("songs", randomSongs);
        model.addAttribute("artists", randomArtists);
        model.addAttribute("genres", randomGenres);
        model.addAttribute("searchObject", new SearchObject());
        return "home";
    }

    @GetMapping("/result")
    public String resultView(@ModelAttribute SearchObject searchObject, BindingResult error, Model model){
        ArrayList<Song> resultSongs = musicRepository.getSongsBySearch(searchObject.getName());
        model.addAttribute("searchedFor", "Search results for: " + searchObject.getName());
        model.addAttribute("resultSongs", resultSongs);
        return "results";
    }
}
