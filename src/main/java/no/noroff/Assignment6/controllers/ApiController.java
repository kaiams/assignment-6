package no.noroff.Assignment6.controllers;

import no.noroff.Assignment6.data_access.CustomerRepository;
import no.noroff.Assignment6.models.Customer;
import no.noroff.Assignment6.models.CustomerCountry;
import no.noroff.Assignment6.models.CustomerFavouriteGenre;
import no.noroff.Assignment6.models.CustomerSpending;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ApiController {
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customers")
    public ArrayList<Customer> getCustomerByName(@RequestParam(value = "name", required = false) String name,
                                                 @RequestParam(value = "limit", required = false) Integer limit,
                                                 @RequestParam(value = "offset", required = false) Integer offset) {

        if (limit != null && offset != null) {
            return customerRepository.getCustomersWithLimitAndOffset(limit, offset);
        }
        else if (limit != null) {
            return customerRepository.getCustomersWithLimitAndOffset(limit, 0);
        }
        else if (offset != null){
            return customerRepository.getCustomersWithLimitAndOffset(250, offset);
        }
        else if (name != null) {
            return customerRepository.getCustomersByName(name);
        }
        return customerRepository.getAllCustomers();
    }

    @GetMapping("/customers/{id}")
    public Customer getCustomer(@PathVariable int id) {
        return customerRepository.getCustomerById(id);
    }

    @PostMapping("/customers")
    public Customer addCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    @PatchMapping("/customers/{id}")
    public Customer updateCustomer(@PathVariable int id, @RequestBody Customer updatedCustomer){
        return customerRepository.updateCustomer(id, updatedCustomer);
    }

    @GetMapping("/customers/country-to-customer")
    public ArrayList<CustomerCountry> getNumberOfCustomersToCountry(){
        return customerRepository.getNumberOfCustomersToCountry();
    }

    @GetMapping("/customers/highest-spenders")
    public ArrayList<CustomerSpending> getHighestSpenders(){
        return customerRepository.getHighestSpenders();
    }

    @GetMapping("/customers/{id}/favourite-genre")
    public ArrayList<CustomerFavouriteGenre> getFavouriteGenreByCustomerId(@PathVariable int id){
        return customerRepository.getFavouriteGenreByCustomerId(id);
    }

}
