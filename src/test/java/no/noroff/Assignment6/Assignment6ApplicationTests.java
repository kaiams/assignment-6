package no.noroff.Assignment6;

import no.noroff.Assignment6.data_access.CustomerRepository;
import no.noroff.Assignment6.models.Customer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Assignment6ApplicationTests {
	@Autowired
	CustomerRepository customerRepository;

	@Test
	@DisplayName("Add Customer")
	void addCustomer_addsCustomerToDatabaseAndReturnsNewlyCreatedEntry() {
		Customer newCustomer = new Customer(
				0,
				"Kaia",
				"Bitch",
				"Norway",
				"0485",
				"48387350",
				"johannesbrochdue@hotmail.com");
		Customer actual = customerRepository.addCustomer(newCustomer);
		assertEquals(newCustomer, actual);
	}


	@Test
	@DisplayName("Update Customer")
	void updateCustomer_returnsUpdatedCustomerWithProperValues() {
		Customer updateCustomer = new Customer(
				60,
				"Johannes",
				"Bitch",
				"Norway",
				"0485",
				"48387350",
				"johannesbrochdue@hotmail.com");
		Customer actual = customerRepository.updateCustomer(60, updateCustomer);
		assertEquals(updateCustomer, actual);
	}
}
