FROM openjdk:16
ADD target/Assignment6-0.0.1-SNAPSHOT.jar Assignment6.jar
ENTRYPOINT [ "java", "-jar", "Assignment6.jar" ]