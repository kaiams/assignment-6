# Assignment 6
Provides some endpoints for the Chinook SQLite database. 
There is also a Thymeleaf application for displaying 5 random artists, songs and genres
with opportunity to search for songtitles.

[Deployed with Heroku](https://the-international-music-disc.herokuapp.com/)

## Documentation

<ins> /customers:</ins>&nbsp;

**Get**&nbsp;
Returns all customers in the database

**Post**&nbsp; Adds a new customer

**Queries:**&nbsp;

**name**
Returns customers with firstname equal to parameter &nbsp;

**limit**
Specifies number of customers returned in query. Default 250&nbsp;

**offset**
Specifies number of customers to skip in query. Default 0 &nbsp;

<ins> /customers/{id}:</ins>&nbsp;

**Get**
Returns customer with provided id&nbsp; 
**Patch**
Updates customer with provided id. Requires a full customer body&nbsp;

<ins>/customers/country-to-customer:</ins>&nbsp;

**Get**
Returns countries sorted by number of customers in descending order&nbsp;

<ins>/customers/highest-spenders:</ins>&nbsp;

**Get**
Returns customers sorted by their total spending in descending order&nbsp;

<ins>/customers/{id}/favourite-genre:</ins>&nbsp;

**Get**
Returns the favourite genre of provided customerId based on purchases &nbsp;

## Contributors
- Johannes Broch-Due
- Kaia Marie Strøm